name := "farandulapp"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test",
  "org.mockito" % "mockito-all" % "1.9.0" % "test",
  "org.eclipse.jetty" % "jetty-servlet" % "8.1.0.RC5",
  "javax.servlet" % "servlet-api" % "2.5",
  "se.radley" %% "play-plugins-salat" % "1.4.0",
  "org.mongodb" %% "casbah" % "2.5.0",
  "com.novus" %% "salat" % "1.9.2",
  "org.ocpsoft.prettytime" % "prettytime" % "3.1.0.Final"
)

play.Project.playScalaSettings