package controllers

import play.api._
import play.api.mvc._

object Application extends Controller {

  def index = Action {
    Ok(views.html.index.render())
  }

  def users = Action {
    Ok("Method hit is here")
  }

}