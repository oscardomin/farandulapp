package models

/**
 * Created by oscar on 05/02/14.
 */


import java.util.Date
import com.novus.salat._
import com.novus.salat.annotations._
import com.novus.salat.dao._
import com.mongodb.casbah.Imports._
import se.radley.plugin.salat._
import mongoContext._

case class User(
   id: ObjectId = new ObjectId,
   username: String,
   password: String,
   address: Option[Address] = None,
   added: Date = new Date(),
   updated: Option[Date] = None,
   deleted: Option[Date] = None,
   @Key("company_id")company: Option[ObjectId] = None
)

object User {

}
